require "rails_helper"

RSpec.describe CustomerMailer, :type => :mailer do

  let(:department) { FactoryGirl.create(:department) }
  let(:ticket) { FactoryGirl.create :ticket, department: department }

  context ".new_ticket_email" do

    let(:mail) { CustomerMailer.new_ticket_email(ticket) }

    it "notifies the correct customer" do
      expect(mail.to).to eq([ticket.customer_email])
    end

    it "sends its reference number" do
      expect(mail.body.encoded).to match("Reference number: #{ticket.ref_num}")
    end

    it "sends its ticket page" do
      expect(mail.body.encoded).to match("http://example.com/tickets/#{ticket.ref_num}/edit")
    end

  end

  context ".ticket_updated", focus: true do

    let(:admin) { FactoryGirl.create(:admin) }

    before do
      ticket.update replies_attributes: {
        '0' => {
          description: "Admin's reply",
          admin_id: admin.id,
        },
        '1' => {
          description: "Customer reply",
          customer_email: ticket.customer_email
        },
        '2' => {
          description: "Admin's second reply",
          admin_id: admin.id,
        },
      }
    end

    let(:mail) { CustomerMailer.ticket_updated_email(ticket) }

    it "notifies the correct customer" do
      expect(mail.to).to eq([ticket.customer_email])
    end

    it "sends its reference number" do
      expect(mail.body.encoded).to match("Reference number: #{ticket.ref_num}")
    end

    it "sends its ticket page" do
      expect(mail.body.encoded).to match("http://example.com/tickets/#{ticket.ref_num}/edit")
    end

    it "sends the reply" do
      expect(mail.body.encoded).to match("Admin's reply")
      expect(mail.body.encoded).to match("Customer reply")
      expect(mail.body.encoded).to match("Admin's second reply")
    end

  end

end
