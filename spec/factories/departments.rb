FactoryGirl.define do
  factory :department do
    name { ["Help & Support", "Administrative area", "Technical team"].sample }
  end

end
