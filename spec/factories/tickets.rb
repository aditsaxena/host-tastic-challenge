FactoryGirl.define do
  factory :ticket do
    sequence :name do |n|
      "Customer Name #{n}"
    end

    sequence :customer_email do |n|
      "customer_#{n}@gmail.com"
    end

    department nil
    admin nil

    sequence :subject do |n|
      "Subject #{n}"
    end

    description "This is my ticket"
    status nil

    ref_num "ABC-4F-ABC-8D-ABC"
  end

end
