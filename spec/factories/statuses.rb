FactoryGirl.define do
  factory :status do
    name { ['Waiting for Staff Response', 'Waiting for Customer', 'On Hold', 'Cancelled', 'Completed'].sample }
  end

end
