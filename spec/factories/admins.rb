FactoryGirl.define do

  factory :admin do
    sequence(:name)  { |n| "Admin #{n}" }
    sequence(:email) { |n| "admin_#{n}@example.com"}
    password "foobar33Aag"
    password_confirmation "foobar33Aag"
  end

end
