require 'rails_helper'

RSpec.describe Reply, :type => :model do

  context "validation" do

    subject(:reply) { Reply.new ticket_id: 22, description: "This is the description" }

    context "must have an author" do

      before do
        expect(reply.valid?).not_to eq true
        expect(reply.errors.messages.keys).to match_array [:admin_id, :customer_email]
      end

      after do
        expect(reply.valid?).to eq true
      end

      it "author can be a customer" do
        reply.customer_email = "customer_email@example.com"
      end

      it "author can be an admin" do
        reply.admin = mock_model(Admin)
      end

    end

  end

end
