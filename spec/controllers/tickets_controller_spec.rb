require 'rails_helper'

RSpec.describe TicketsController, :type => :controller do

  let(:department) { FactoryGirl.create(:department) }

  let(:valid_attributes) {
    FactoryGirl.attributes_for :ticket, department_id: department.id
  }

  let(:valid_session) { {} }

  describe "GET new" do
    it "assigns a new ticket as @ticket" do
      get :new, {}, valid_session
      expect(assigns(:ticket)).to be_a_new(Ticket)
    end
  end

  describe "GET edit" do
    it "assigns the requested ticket as @ticket" do
      ticket = Ticket.create! valid_attributes
      get :edit, {:id => ticket.to_param}, valid_session
      expect(assigns(:ticket)).to eq(ticket)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Ticket" do
        expect {
          post :create, {:ticket => valid_attributes}, valid_session
        }.to change(Ticket, :count).by(1)
      end

      it "assigns a newly created ticket as @ticket" do
        post :create, {:ticket => valid_attributes}, valid_session
        expect(assigns(:ticket)).to be_a(Ticket)
        expect(assigns(:ticket)).to be_persisted
      end

      it "delivers an email after ticket creation" do
        expect { post :create, {:ticket => valid_attributes} }.to change { ActionMailer::Base.deliveries.count }.by(1)
      end

    end

  end

  describe "PUT update" do
    describe "with valid params" do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      it "updates the requested ticket" do
        ticket = Ticket.create! valid_attributes
        put :update, {:id => ticket.to_param, :ticket => new_attributes}, valid_session
        ticket.reload
        skip("Add assertions for updated state")
      end

      it "assigns the requested ticket as @ticket" do
        ticket = Ticket.create! valid_attributes
        put :update, {:id => ticket.to_param, :ticket => valid_attributes}, valid_session
        expect(assigns(:ticket)).to eq(ticket)
      end

    end

  end

end
