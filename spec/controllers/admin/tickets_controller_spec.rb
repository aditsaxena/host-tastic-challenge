require 'rails_helper'

RSpec.describe Admin::TicketsController, :type => :controller do

  login_admin

  let(:department) { FactoryGirl.create(:department) }

  let(:valid_attributes) {
    FactoryGirl.attributes_for :ticket, department_id: department.id
  }

  let(:valid_attributes_with_reply) {
    params_with_reply = valid_attributes.dup
    params_with_reply[:replies_attributes] = {
      '0' => {
        description: "Admin's reply",
        admin_id: subject.current_admin.id,
      }
    }
    params_with_reply
  }

  describe "GET new" do
    it "assigns a new ticket as @ticket" do
      get :new, {}
      expect(assigns(:ticket)).to be_a_new(Ticket)
    end
  end

  describe "GET edit" do
    it "assigns the requested ticket as @ticket" do
      ticket = Ticket.create! valid_attributes
      get :edit, {:id => ticket.to_param}
      expect(assigns(:ticket)).to eq(ticket)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Ticket" do
        expect {
          post :create, {:ticket => valid_attributes}
        }.to change(Ticket, :count).by(1)
      end

      it "assigns a newly created ticket as @ticket" do
        post :create, {:ticket => valid_attributes}
        expect(assigns(:ticket)).to be_a(Ticket)
        expect(assigns(:ticket)).to be_persisted
      end

      it "delivers an email after ticket creation" do
        expect { post :create, {:ticket => valid_attributes} }.to change { ActionMailer::Base.deliveries.count }.by(1)
      end

    end

  end

  describe "PUT update" do
    describe "with valid params" do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      it "updates the requested ticket" do
        ticket = Ticket.create! valid_attributes
        put :update, {:id => ticket.to_param, :ticket => new_attributes}
        ticket.reload
        skip("Add assertions for updated state")
      end

      it "assigns the requested ticket as @ticket" do
        ticket = Ticket.create! valid_attributes
        put :update, {:id => ticket.to_param, :ticket => valid_attributes}
        expect(assigns(:ticket)).to eq(ticket)
      end

      context "delivers an email to customer" do
        it "not on a save without reply" do
          ticket = Ticket.create! valid_attributes
          expect { put :update, { :id => ticket.to_param, :ticket => valid_attributes} }.not_to change { ActionMailer::Base.deliveries.count }
        end

        it "on a reply message" do
          ticket = Ticket.create! valid_attributes
          expect { put :update, { :id => ticket.to_param, :ticket => valid_attributes_with_reply} }.to change { ActionMailer::Base.deliveries.count }.by(1)
        end

      end

    end

  end

end
