require 'rails_helper'

RSpec.describe ReferenceNumber do

  context "generates a reference number" do

    it "is in valid format" do
      expect(ReferenceNumber.new.generate).to match(/^[0-9A-Z]{3}-\h{2}-[0-9A-Z]{3}-\h{2}-[0-9A-Z]{3}$/)
    end

  end

end
