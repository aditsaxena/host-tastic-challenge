json.array!(@tickets) do |ticket|
  json.extract! ticket, :id, :name, :customer_email, :department_id, :admin_id, :description, :status_id, :ref_num
  json.url ticket_url(ticket, format: :json)
end
