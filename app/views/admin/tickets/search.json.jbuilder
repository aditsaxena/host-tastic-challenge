json.elements do

  json.array!(@tickets) do |ticket|

    # json.extract! ticket, :id
    json.id ticket.ref_num
    json.name ticket.subject

  end

end
