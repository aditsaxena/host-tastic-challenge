class TicketSearcher

  def initialize(query, limit: 10)
    @query = query
    @limit = limit
  end

  def search
    found_elements = PgSearch.multisearch(@query)

    tickets_ids = []
    replies_ids = []

    found_elements.each do |found_element|
      if found_element.searchable_type == "Ticket"
        tickets_ids << found_element.searchable_id
      else
        replies_ids << found_element.searchable_id
      end
    end

   Ticket.joins(:replies).where("tickets.id IN (?) OR replies.id IN (?)", tickets_ids, replies_ids).limit(@li)
  end

end
