class Ticket < ActiveRecord::Base

  include PgSearch

  multisearchable :against => [:name, :customer_email, :subject, :description]

  belongs_to :department
  belongs_to :admin
  belongs_to :status

  has_many :replies

  validates_presence_of :name, :department, :subject, :description
  validates :customer_email, format: /@/

  before_create :generate_ref_num
  before_create :set_base_status

  accepts_nested_attributes_for :replies

  def self.unassigned
    self.where(admin: nil)
  end

  def self.open_tickets
    self.joins(:status).where("statuses.name IN ('Waiting for Staff Response', 'Waiting for Customer', 'On Hold')")
  end

  def self.on_hold
    self.joins(:status).where("statuses.name = 'On Hold'")
  end

  def self.closed
    self.joins(:status).where("statuses.name IN ('Cancelled', 'Completed')")
  end

  def to_param
    ref_num
  end

  private

  def generate_ref_num
    self.ref_num = ReferenceNumber.new.generate
  end

  def set_base_status
    self.status = Status.waiting_for_staff_response
  end

end
