class Status < ActiveRecord::Base

  has_many :tickets
  validates_presence_of :name

  def self.waiting_for_staff_response
    find_by name: 'Waiting for Staff Response'
  end

  def self.waiting_for_customer_response
    find_by name: 'Waiting for Customer'
  end

  def self.on_hold
    find_by name: 'On Hold'
  end

  def self.cancelled
    find_by name: 'Cancelled'
  end

  def self.completed
    find_by name: 'Completed'
  end

  def closed?
    name.in? %W(Cancelled Completed)
  end

end
