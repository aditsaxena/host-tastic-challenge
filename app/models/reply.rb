class Reply < ActiveRecord::Base

  include PgSearch

  multisearchable :against => [:description]

  belongs_to :ticket
  belongs_to :admin

  validates_presence_of :description

  validate :validates_author

  def validates_author
    if admin_id.blank? && customer_email.blank?
      message = "can't be blank"
      self.errors[:admin_id] << message
      self.errors[:customer_email] << message
    end
  end

  def author
    customer_email || admin.name
  end

end
