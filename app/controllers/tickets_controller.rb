class TicketsController < ApplicationController
  before_action :set_ticket, only: [:edit, :update, :complete, :cancel]

  respond_to :html

  def show
    respond_with(@ticket)
  end

  def new
    @ticket = Ticket.new
    respond_with(@ticket)
  end

  def edit
  end

  def create
    @ticket = Ticket.new(ticket_params)
    @ticket.save
    if @ticket.save
      CustomerMailer.new_ticket_email(@ticket).deliver

      flash[:notice] = 'Your feedback was successfully saved.'
      respond_with(@ticket, :status => :created, :location => @ticket) do |format|
        format.html { redirect_to edit_ticket_path(@ticket) }
      end
    else
      respond_with(@ticket.errors, :status => :unprocessable_entity) do |format|
        format.html { render :action => :new }
      end
    end
  end

  def update
    @ticket.update(reply_params)

    respond_to do |format|
      if @ticket.save
        flash[:notice] = 'Your feedback was successfully saved.'
        format.html { redirect_to edit_ticket_path(@ticket) }
      else
        flash[:notice] = 'Some error prevented the form to submit.'
        format.html { render 'edit' }
      end
    end
  end

  def cancel
    @ticket.update(status: Status.cancelled)
    redirect_to edit_ticket_path(@ticket)
  end

  def complete
    @ticket.update(status: Status.completed)
    redirect_to edit_ticket_path(@ticket)
  end

  private
    def set_ticket
      @ticket = Ticket.find_by(ref_num: params[:id]) or raise "Not found"
    end

    def ticket_params
      params.require(:ticket).permit(:name, :customer_email, :department_id, :subject, :description)
    end

    def reply_params
      params.require(:ticket).permit(replies_attributes: [:description, :customer_email])
    end
end
