class Admin

  class TicketsController < AdminController
    before_action :set_ticket, only: [:show, :edit, :update, :destroy]
    before_action :set_section

    respond_to :html

    def index
      @tickets = Ticket.all.includes(:admin, :department).unassigned
      list_tickets
    end

    def open
      @section = params[:action]
      @tickets = Ticket.all.includes(:admin, :department).open_tickets
      list_tickets
    end

    def on_hold
      @section = params[:action]
      @tickets = Ticket.all.includes(:admin, :department).on_hold
      list_tickets
    end

    def closed
      @section = params[:action]
      @tickets = Ticket.all.includes(:admin, :department).closed
      list_tickets
    end

    def search
      @tickets = TicketSearcher.new(params[:q], limit: params[:page_limit]).search
    end

    def show
      respond_with(@ticket)
    end

    def new
      @ticket = Ticket.new
      respond_with(@ticket)
    end

    def edit
    end

    def create
      @ticket = Ticket.new(ticket_params)
      if @ticket.save
        CustomerMailer.new_ticket_email(@ticket).deliver
        flash[:notice] = 'Your feedback was successfully saved.'
        respond_with(@ticket, :status => :created, :location => @ticket) do |format|
          format.html { redirect_to edit_admin_ticket_path(@ticket) }
        end
      else
        respond_with(@ticket.errors, :status => :unprocessable_entity) do |format|
          format.html { render :action => :new }
        end
      end
    end

    def update
      tickt_par = ticket_params.dup
      tickt_par['replies_attributes'] = Hash[tickt_par['replies_attributes'].reject { |k, v| v['description'].blank? }] if tickt_par['replies_attributes']
      @ticket.update(tickt_par)
      if @ticket.save
        send_email_if_replied(tickt_par['replies_attributes'])
        flash[:notice] = 'Your feedback was successfully saved.'
        respond_with(@ticket, :status => :created, :location => @ticket) do |format|
          format.html { redirect_to edit_admin_ticket_path(@ticket) }
        end
      else
        respond_with(@ticket.errors, :status => :unprocessable_entity) do |format|
          format.html { render :action => :new }
        end
      end
    end

    def destroy
      @ticket.destroy
      respond_with(@ticket)
    end

    private

      def send_email_if_replied(reply_params)
        if reply_params
          CustomerMailer.ticket_updated_email(@ticket).deliver
        end
      end

      def set_section
        @section = nil
      end

      def list_tickets
        respond_to do |format|
          format.html { render :index }
          format.json { render json: @tickets }
        end
      end

      def set_ticket
        @ticket = Ticket.find_by(ref_num: params[:id]) or raise "Not found"
      end

      def ticket_params
        params.require(:ticket).permit(:name, :customer_email, :department_id, :admin_id, :subject, :description, :status_id, replies_attributes: [:description, :admin_id])
      end
  end

end