class CustomerMailer < ActionMailer::Base
  default from: "from@example.com"

  def new_ticket_email(ticket)
    @ticket = ticket
    send_email 'New Ticket - My Awesome Site'
  end

  def ticket_updated_email(ticket)
    @ticket = ticket
    send_email 'Your Ticket has been updated - My Awesome Site'
  end

  def send_email(subject)
    @url = "http://example.com/tickets/#{@ticket.ref_num}/edit"
    mail(to: @ticket.customer_email, subject: subject)
  end

end
