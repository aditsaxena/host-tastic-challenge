module AdminHelper

  def show_menu_items
    {
      index: {
        title: "New unassigned tickets",
        url: admin_tickets_path,
      },
      open: {
        title: "Open Tickets",
        url: open_admin_tickets_path,
      },
      on_hold: {
        title: "On hold tickets",
        url: on_hold_admin_tickets_path,
      },
      closed: {
        title: "Closed Tickets",
        url: closed_admin_tickets_path,
      },
    }.map do |key, attributes|
      active = key == @section.to_s.to_sym ? 'active' : ''
      content_tag :li, class: "#{active}" do
        link_to attributes[:title], attributes[:url]
      end
    end.join("")
  end

end
