module ApplicationHelper

  def nl2br(string)
    string.gsub("\n", "<br>").html_safe
  end

end
