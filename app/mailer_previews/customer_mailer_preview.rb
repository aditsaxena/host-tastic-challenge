class CustomerMailerPreview < ActionMailer::Preview

  def new_ticket_email
    CustomerMailer.new_ticket_email Ticket.first
  end

  def ticket_updated_email
    CustomerMailer.ticket_updated_email Ticket.first
  end

end
