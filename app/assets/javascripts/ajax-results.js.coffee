$(document).ready ->
  $select = $(".ajax-results")
  return unless $select.length > 0

  select_data = $select.data()

  $select.select2(
    placeholder: "Search"
    minimumInputLength: 1
    allowClear: true
    ajax:
      url: "#{select_data.path}.json"
      dataType: "json"
      data: (term, page) ->
        q: term
        page_limit: 10

      results: (data, page) ->
        results: data.elements

    formatResult: (element) ->
      """<div data-id="#{element.id}">#{element.name}</div>"""

    formatSelection: (element) ->
      element.name

    initSelection: (element, callback) ->
      el = $(element)
      name = el.data('name') || el.val()
      data = { id: el.val(), name: name }
      callback data
      return

  ).on "change", (e) ->
    if select_data.redirectTo
      url_tpl = select_data.redirectTo.replace('{', '<%=').replace('}', '%>') # fix "{id}" -> "<%=id%>" so it doesn't clash with angular's mustache syntax
      url = _.template url_tpl, id: e.val
      window.location.href = url
    return
