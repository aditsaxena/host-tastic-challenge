require 'securerandom'

class ReferenceNumber

  def generate
    loop do
      ref_num = _random_code_generator
      break ref_num unless Ticket.exists?(ref_num: ref_num)
    end
  end

  def _random_code_generator
    string = SecureRandom.hex(7).upcase
    "#{string[0..2]}-#{string[3..4]}-#{string[5..7]}-#{string[8..9]}-#{string[10..12]}"
  end

end
