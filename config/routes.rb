Rails.application.routes.draw do

  resources :tickets, only: [:new, :create, :edit, :update] do
    member do
      get :cancel
      get :complete
    end
  end

  devise_for :admins
  root 'home#index'

  namespace :admin do
    resources :tickets, only: [:new, :create, :edit, :update, :destroy, :index] do
      collection do
        get :open
        get :on_hold
        get :closed
        get :search
      end
    end
  end

end
