class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :ref_num
      t.string :name
      t.string :customer_email
      t.references :department, index: true
      t.references :admin, index: true
      t.text :description
      t.references :status, index: true

      t.timestamps
    end
  end
end
