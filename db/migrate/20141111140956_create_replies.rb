class CreateReplies < ActiveRecord::Migration
  def change
    create_table :replies do |t|
      t.text :description
      t.references :ticket
      t.references :author, polymorphic: true, index: true

      t.timestamps
    end
  end
end
