class ChangeAuthorTypeInReply < ActiveRecord::Migration
  def change
    remove_column :replies, :author_id, :integer
    remove_column :replies, :author_type, :integer

    change_table :replies do |t|
      t.references :admin, index: true
      t.string :customer_email, index: true
    end

  end
end
