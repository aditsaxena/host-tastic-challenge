class ChangeStatusType < ActiveRecord::Migration
  def change
    remove_column :tickets, :status_id, :text

    change_table :tickets do |t|
      t.references :status, index: true
    end
  end
end
