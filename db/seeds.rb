# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

default_admin = FactoryGirl.create :admin, name: "Admin", email: 'demo@example.com', password: 'demodemo', password_confirmation: 'demodemo'
james = FactoryGirl.create :admin, name: "James", email: 'james@example.com', password: 'demodemo', password_confirmation: 'demodemo'
mike = FactoryGirl.create :admin, name: "Mike", email: 'mike@example.com', password: 'demodemo', password_confirmation: 'demodemo'

help_and_support = FactoryGirl.create :department, name: "Help & Support"
administrative_area = FactoryGirl.create :department, name: "Administrative area"
tech_team = FactoryGirl.create :department, name: "Technical team"

wfor_staff = FactoryGirl.create :status, name: 'Waiting for Staff Response'
wfor_cust = FactoryGirl.create :status, name: 'Waiting for Customer'
on_hold = FactoryGirl.create :status, name: 'On Hold'
cancelled = FactoryGirl.create :status, name: 'Cancelled'
completed = FactoryGirl.create :status, name: 'Completed'

FactoryGirl.create :ticket, description: "Ticket wfor_staff", status: wfor_staff, department: Department.all.sample
FactoryGirl.create :ticket, description: "Ticket wfor_cust", status: wfor_cust, admin: mike, department: Department.all.sample
FactoryGirl.create :ticket, description: "Ticket on_hold", status: on_hold, admin: mike, department: Department.all.sample
FactoryGirl.create :ticket, description: "Ticket cancelled", status: cancelled, admin: james, department: Department.all.sample
FactoryGirl.create :ticket, description: "Ticket completed", status: completed, department: Department.all.sample
